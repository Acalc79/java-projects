package pl.poznan.iilo.adamkucz.guicalculator.reference;

import java.util.Optional;

public class Calculator {
    private double ans;
    private CalculatorState state;

    public Calculator() {
        ans = 0;
        state = CalculatorState.READY;
    }

    public CalculatorState getState() {
        return state;
    }

    public boolean isRunning() {
        return state != CalculatorState.DIV0_ERROR;
    }

    public Optional<Double> getAns() {
        if (isRunning()) {
            return Optional.of(ans);
        } else {
            return Optional.empty();
        }
    }

    public void setState(CalculatorState newState) {
        if (isRunning()) {
            state = newState;
        }
    }

    public void doOperation(double other) {
        switch (state) {
            case READY:
                ans = other;
                break;
            case ADDING:
                ans += other;
                break;
            case SUBTRACTING:
                ans -= other;
                break;
            case MULTIPLYING:
                ans *= other;
                break;
            case DIVIDING:
                if (other == 0) {
                    state = CalculatorState.DIV0_ERROR;
                } else {
                    ans /= other;
                }
                break;
        }
        if (state != CalculatorState.DIV0_ERROR) {
            state = CalculatorState.READY;
        }
    }
}
