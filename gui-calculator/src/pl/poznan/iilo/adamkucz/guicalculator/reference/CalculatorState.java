package pl.poznan.iilo.adamkucz.guicalculator.reference;

public enum CalculatorState {
    READY, ADDING, SUBTRACTING, MULTIPLYING, DIVIDING, DIV0_ERROR
}
