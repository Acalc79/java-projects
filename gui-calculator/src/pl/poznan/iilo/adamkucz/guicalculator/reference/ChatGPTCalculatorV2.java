package pl.poznan.iilo.adamkucz.guicalculator.reference;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ChatGPTCalculatorV2 extends JFrame {

    private double operand1 = 0.0;
    private double operand2 = 0.0;
    private char operator = ' ';

    private JTextField display;
    private JButton[] numberButtons;
    private JButton[] operatorButtons;
    private JButton clearButton;
    private JButton equalsButton;

    public ChatGPTCalculatorV2() {
        // Set up the frame
        setTitle("Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set up the display
        display = new JTextField();
        display.setEditable(false);
        display.setHorizontalAlignment(JTextField.RIGHT);

        // Set up the buttons
        numberButtons = new JButton[10];
        for (int i = 0; i < numberButtons.length; i++) {
            numberButtons[i] = new JButton(Integer.toString(i));
        }

        operatorButtons = new JButton[5];
        operatorButtons[0] = new JButton("+");
        operatorButtons[1] = new JButton("-");
        operatorButtons[2] = new JButton("*");
        operatorButtons[3] = new JButton("/");
        operatorButtons[4] = new JButton("^");

        clearButton = new JButton("C");
        equalsButton = new JButton("=");

        // Set up the layout
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 4;
        add(display, c);

        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridwidth = 1;
        c.gridheight = 1;

        for (int i = 1; i <= 9; i++) {
            c.gridx = (i - 1) % 3;
            c.gridy = (i - 1) / 3 + 1;
            add(numberButtons[i], c);
        }

        c.gridx = 1;
        c.gridy = 4;
        add(numberButtons[0], c);

        c.gridx = 3;
        c.gridy = 1;
        add(operatorButtons[0], c);

        c.gridx = 3;
        c.gridy = 2;
        add(operatorButtons[1], c);

        c.gridx = 3;
        c.gridy = 3;
        add(operatorButtons[2], c);

        c.gridx = 3;
        c.gridy = 4;
        add(operatorButtons[3], c);

        c.gridx = 2;
        c.gridy = 4;
        add(clearButton, c);

        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 1;
        add(equalsButton, c);

        c.gridx = 4;
        c.gridy = 2;
        c.gridheight = 3;
        add(operatorButtons[4], c);

        // Add action listeners to the buttons
        for (JButton button : numberButtons) {
            button.addActionListener(new NumberButtonListener());
        }

        for (JButton button : operatorButtons) {
            button.addActionListener(new OperatorButtonListener());
        }

        clearButton.addActionListener(new ClearButtonListener());
        equalsButton.addActionListener(new EqualsButtonListener());

        // Set the size and show the frame
        pack();
        setVisible(true);
    }

    // Action listener for number buttons
    private class NumberButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String digit = e.getActionCommand();
            display.setText(display.getText() + digit);
        }
    }

    // Action listener for operator buttons
    private class OperatorButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String op = e.getActionCommand();
            if (operator == ' ') {
                operand1 = Double.parseDouble(display.getText());
                operator = op.charAt(0);
                display.setText("");
            } else {
                operand2 = Double.parseDouble(display.getText());
                double result = calculate();
                display.setText(Double.toString(result));
                operand1 = result;
                operator = op.charAt(0);
            }
        }
    }

    // Action listener for clear button
    private class ClearButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            operand1 = 0.0;
            operand2 = 0.0;
            operator = ' ';
            display.setText("");
        }
    }

    // Action listener for equals button
    private class EqualsButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            operand2 = Double.parseDouble(display.getText());
            double result = calculate();
            display.setText(Double.toString(result));
            operand1 = 0.0;
            operand2 = 0.0;
            operator = ' ';
        }
    }

    // Calculate the result of the current operation
    private double calculate() {
        double result = 0.0;
        switch (operator) {
            case '+':
                result = operand1 + operand2;
                break;
            case '-':
                result = operand1 - operand2;
                break;
            case '*':
                result = operand1 * operand2;
                break;
            case '/':
                result = operand1 / operand2;
                break;
            case '^':
                result = Math.pow(operand1, operand2);
                break;
        }
        return result;
    }

    // Main method to start the program
    public static void main(String[] args) {
        ChatGPTCalculatorV2 calculator = new ChatGPTCalculatorV2();
    }
}

