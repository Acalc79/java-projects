package pl.poznan.iilo.adamkucz.guicalculator.reference;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ChatGPTCalculator {
    private JFrame frame;
    private JTextField display;
    private JButton[] numberButtons;
    private JButton[] operatorButtons;
    private JButton equalsButton, clearButton;
    private double operand1 = 0.0, operand2 = 0.0;
    private char operator;

    public ChatGPTCalculator() {
        frame = new JFrame("Calculator");
        display = new JTextField(10);
        display.setEditable(false);

        numberButtons = new JButton[10];
        for (int i = 0; i < numberButtons.length; i++) {
            String number = Integer.toString(i);
            numberButtons[i] = new JButton(number);
            numberButtons[i].addActionListener(new NumberButtonListener(number));
        }

        operatorButtons = new JButton[5];
        operatorButtons[0] = new JButton("+");
        operatorButtons[1] = new JButton("-");
        operatorButtons[2] = new JButton("*");
        operatorButtons[3] = new JButton("/");
        operatorButtons[4] = new JButton("^");

        for (JButton operatorButton : operatorButtons) {
            operatorButton.addActionListener(new OperatorButtonListener(operatorButton.getText().charAt(0)));
        }

        equalsButton = new JButton("=");
        clearButton = new JButton("C");

        equalsButton.addActionListener(new EqualsButtonListener());
        clearButton.addActionListener(new ClearButtonListener());

        // Add components to the frame
        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayout(4, 3));
        for (JButton button : numberButtons) {
            panel1.add(button);
        }
        panel1.add(clearButton);
        panel1.add(numberButtons[0]);
        panel1.add(equalsButton);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayout(5, 1));
        for (JButton operatorButton : operatorButtons) {
            panel2.add(operatorButton);
        }

        JPanel panel3 = new JPanel();
        panel3.setLayout(new BorderLayout());
        panel3.add(display, BorderLayout.NORTH);
        panel3.add(panel1, BorderLayout.CENTER);
        panel3.add(panel2, BorderLayout.EAST);

        frame.add(panel3);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    // Action listener for number buttons
    private class NumberButtonListener implements ActionListener {
        private String number;

        public NumberButtonListener(String number) {
            this.number = number;
        }

        public void actionPerformed(ActionEvent event) {
            display.setText(display.getText() + number);
        }
    }

    // Action listener for operator buttons
    private class OperatorButtonListener implements ActionListener {
        private char operator;

        public OperatorButtonListener(char operator) {
            this.operator = operator;
        }

        public void actionPerformed(ActionEvent event) {
            operand1 = Double.parseDouble(display.getText());
            display.setText("");
            ChatGPTCalculator.this.operator = operator;
        }
    }

    // Action listener for equals button
    private class EqualsButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            operand2 = Double.parseDouble(display.getText());
            double result = 0.0;
            switch (operator) {
                case '+':
                    result = operand1 + operand2;
                    break;
                case '-':
                    result = operand1 - operand2;
                    break;
                case '*':
                    result = operand1 * operand2;
                    break;
                case '/':
                    result = operand1 / operand2;
                    break;
                case '^':
                    result = Math.pow(operand1, operand2);
                    break;
            }
            display.setText(Double.toString(result));
        }
    }

    // Action listener for clear button
    private class ClearButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            display.setText("");
            operand1 = 0.0;
            operand2 = 0.0;
            operator = ' ';
        }
    }

    public static void main(String[] args) {
        ChatGPTCalculator calculator = new ChatGPTCalculator();
    }
}
