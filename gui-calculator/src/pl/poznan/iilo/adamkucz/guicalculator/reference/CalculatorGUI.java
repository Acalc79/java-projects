package pl.poznan.iilo.adamkucz.guicalculator.reference;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class CalculatorGUI extends JFrame {
    private static final int WIDTH = 350;
    private static final int HEIGHT = 500;
    private static final int FONT_SIZE = 40;

    private JTextField display;
    private Calculator calc;

    public static void main(String[] args) {
        new ChatGPTCalculatorV2();
    }

    public CalculatorGUI() {
        super("Calculator (basic)");
        calc = new Calculator();

        addComponents();
        setSize(WIDTH,HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void addComponents() {
        display = createDisplay();

        JPanel buttonPanel = new JPanel(new GridLayout(4, 4));
        addButtons(buttonPanel);

        JSplitPane content = new JSplitPane(JSplitPane.VERTICAL_SPLIT, display, buttonPanel);
        content.setResizeWeight(0.3);
        content.setDividerSize(0);
        content.setEnabled(false);
        getContentPane().add(content);
    }

    private static JTextField createDisplay() {
        JTextField textField = new JTextField("0");
        textField.setFont(textField.getFont().deriveFont(Font.BOLD, FONT_SIZE));
        textField.setHorizontalAlignment(JTextField.RIGHT);
        return textField;
    }

    private void addButtons(Container buttonPanel) {
        buttonPanel.add(makeDigitButton(7));
        buttonPanel.add(makeDigitButton(8));
        buttonPanel.add(makeDigitButton(9));
        buttonPanel.add(makeButton("/", e -> changeState(CalculatorState.DIVIDING)));

        buttonPanel.add(makeDigitButton(4));
        buttonPanel.add(makeDigitButton(5));
        buttonPanel.add(makeDigitButton(6));
        buttonPanel.add(makeButton("*", e -> changeState(CalculatorState.MULTIPLYING)));

        buttonPanel.add(makeDigitButton(1));
        buttonPanel.add(makeDigitButton(2));
        buttonPanel.add(makeDigitButton(3));
        buttonPanel.add(makeButton("-", e -> changeState(CalculatorState.SUBTRACTING)));

        buttonPanel.add(makeDigitButton(0));
        buttonPanel.add(makeButton(".", e -> {
            if (!display.getText().contains(".")) {
                display.setText(display.getText() + ".");
            }
        }));
        buttonPanel.add(makeButton("+", e -> changeState(CalculatorState.ADDING)));
        buttonPanel.add(makeButton("=", e -> {
            calc.doOperation(Double.parseDouble(display.getText()));
            if (calc.getAns().isPresent()) {
                display.setText(calc.getAns().get().toString());
            } else {
                display.setText("#ERROR/0");
            }
        }));
    }

    private void changeState(CalculatorState newState) {
        if (calc.getState() == CalculatorState.READY) {
            calc.setState(newState);
            display.setText("0");
        }
    }

    private JButton makeDigitButton(int digit) {
        return makeButton(String.valueOf(digit), e -> digitActionListener(digit));
    }

    private JButton makeButton(String text, ActionListener listener) {
        JButton b = new JButton(text);
        b.setFont(b.getFont().deriveFont(Font.BOLD, FONT_SIZE));
        b.addActionListener(e -> {
            System.out.println("Button pressed, internal state: " + calc.getState() + ", " + calc.getAns());
            if (calc.isRunning()) {
                listener.actionPerformed(e);
            }
            System.out.println("Afterwards: " + calc.getState() + ", " + calc.getAns());
        });
        return b;
    }

    private void digitActionListener(int digit) {
        String current = display.getText();
        display.setText((current.equals("0") ? "" : current) + digit);
    }
}
