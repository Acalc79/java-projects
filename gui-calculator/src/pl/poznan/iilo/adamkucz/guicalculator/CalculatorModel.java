package pl.poznan.iilo.adamkucz.guicalculator;

import java.util.Optional;

public class CalculatorModel {
    private double previousAns;
    // represents what operation is in progress or whether the calculator is idle
    private char state; // allowed values: ' ', '+', '-', '*', '/', 'e', '√'

    public Optional<Double> getAns() {
        if (state != 'e') {
            return Optional.of(previousAns);
        } else {
            return Optional.empty();
        }
    }

    public void setAns(double newValue) {
        previousAns = newValue;
    }

    public char getState() {
        return state;
    }

    public void setState(char newState) {
        state = newState;
    }

    public CalculatorModel() {
        previousAns = 0;
        state = ' ';
    }

    public void doOperation(double otherNumber) {
        if (state == '+') {
            previousAns += otherNumber;
        } else if (state == '-') {
            previousAns -= otherNumber;
        } else if (state == '*') {
            previousAns *= otherNumber;
        } else if (state == '√') {
            if (previousAns >= 0) {
                previousAns = Math.sqrt(previousAns);
            } else {
                state = 'e';
            }
        } else if (state == '/') {
            if (otherNumber != 0) {
                previousAns /= otherNumber;
            } else {
                state = 'e'; // for error
            }
        } else if (state == ' ') {
            previousAns = otherNumber;
        }
        if (state != 'e') {
            state = ' ';
        }
    }

    public void doOperationImmediately() {
        if (state == '√') {
            if (previousAns >= 0) {
                previousAns = Math.sqrt(previousAns);
            } else {
                state = 'e';
            }
        }
        if (state != 'e') {
            state = ' ';
        }
    }
}
