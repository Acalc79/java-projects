package pl.poznan.iilo.adamkucz.guicalculator;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

public class CalculatorViewControllerWithSqrt extends JFrame {
    private static final int WIDTH = 500;
    private static final int HEIGHT = 800;
    private static final float FONT_SIZE = 40;

    private JTextField display;
    private CalculatorModel calc;

    public CalculatorViewControllerWithSqrt() {
        super("Calculator");

        calc = new CalculatorModel();

        addComponents();

        setSize(WIDTH, HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void addComponents() {
        Container pane = getContentPane();

        display = new JTextField("0");
        display.setFont(display.getFont().deriveFont(Font.BOLD, FONT_SIZE));
        display.setHorizontalAlignment(JTextField.RIGHT);

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(5, 4));
        JButton[] digits = new JButton[10];
        for (int i = 0; i < digits.length; i++) {
            digits[i] = new JButton(String.valueOf(i));
            final int finalI = i;
            digits[i].addActionListener(e -> digitPressed(finalI));
            digits[i].setFont(digits[i].getFont().deriveFont(Font.BOLD, FONT_SIZE));
        }

        // add, subtract, divide, multiply, equal sign
        char[] binaryOperatorChars = {'+', '-', '*', '/'};
        JButton[] binaryOperators = new JButton[binaryOperatorChars.length];
        for (int i = 0; i < binaryOperators.length; i++) {
            binaryOperators[i] = new JButton(String.valueOf(binaryOperatorChars[i]));
            binaryOperators[i].setFont(binaryOperators[i].getFont().deriveFont(Font.BOLD, FONT_SIZE));
            final int finalI = i;
            binaryOperators[i].addActionListener(e -> {
                if (calc.getState() == ' ') {
                    calc.setState(binaryOperatorChars[finalI]);
                    calc.setAns(Double.parseDouble(display.getText()));
                    display.setText("0");
                }
            });
        }

        JButton sqrtButton = new JButton("√");
        sqrtButton.setFont(sqrtButton.getFont().deriveFont(Font.BOLD, FONT_SIZE));
        sqrtButton.addActionListener(e -> {
            if (calc.getState() == ' ') {
                calc.setAns(Double.parseDouble(display.getText()));
                calc.setState('√');
                calc.doOperationImmediately();
                Optional<Double> maybeAns = calc.getAns();
                if (maybeAns.isPresent()) {
                    display.setText(String.valueOf(maybeAns.get()));
                } else {
                    display.setText("ERR");
                }
            }
        });

        JButton equalButton = new JButton("=");
        equalButton.setFont(equalButton.getFont().deriveFont(Font.BOLD, FONT_SIZE));
        equalButton.addActionListener(e -> {
            calc.doOperation(Double.parseDouble(display.getText()));
            Optional<Double> maybeAns = calc.getAns();
            if (maybeAns.isPresent()) {
                display.setText(String.valueOf(maybeAns.get()));
            } else {
                display.setText("ERR");
            }
        });

        JButton dotButton = new JButton(".");
        dotButton.setFont(dotButton.getFont().deriveFont(Font.BOLD, FONT_SIZE));
        dotButton.addActionListener(e -> {
            String current = display.getText();
            if (!current.contains(".")) {
                display.setText(current + ".");
            }
        });

        buttons.add(digits[7]);
        buttons.add(digits[8]);
        buttons.add(digits[9]);
        buttons.add(binaryOperators[3]);
        buttons.add(digits[4]);
        buttons.add(digits[5]);
        buttons.add(digits[6]);
        buttons.add(binaryOperators[2]);
        buttons.add(digits[1]);
        buttons.add(digits[2]);
        buttons.add(digits[3]);
        buttons.add(binaryOperators[1]);
        buttons.add(digits[0]);
        buttons.add(dotButton);
        buttons.add(binaryOperators[0]);
        buttons.add(equalButton);
        buttons.add(sqrtButton);

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                display,
                buttons);
        split.setDividerSize(0);
        split.setResizeWeight(0.3);
        pane.add(split);
    }

    private void digitPressed(int digit) {
        String current = display.getText();
        if (current.equals("0")) {
            display.setText(String.valueOf(digit));
        } else {
            display.setText(current + digit);
        }
    }

    public static void main(String[] args) {
        new CalculatorViewControllerWithSqrt();
    }
}
