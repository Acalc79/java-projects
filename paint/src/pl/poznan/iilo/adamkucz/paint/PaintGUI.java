package pl.poznan.iilo.adamkucz.paint;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.stream.IntStream;

public class PaintGUI extends JFrame {
    private static final int WIDTH = 500;
    private static final int HEIGHT = 800;
    private static final Color[] COLOURS = {Color.BLACK, Color.BLUE, Color.RED, Color.GREEN};
    private static final ImmutableDimension BUTTON_SIZE = new ImmutableDimension(50, 50);
    private static final int MAX_PEN_SIZE = 50;
    private static final int MIN_PEN_SIZE = 1;
    private Canvas canvas;

    public PaintGUI(boolean simple) {
        super("Paint");

        addComponents(simple);

        pack();
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void addComponents(boolean simple) {
        Container pane = getContentPane();

        canvas = new Canvas(WIDTH, HEIGHT);
        pane.add(canvas, BorderLayout.CENTER);
        pane.add(makeControls(simple), BorderLayout.NORTH);
    }

    private Component makeControls(boolean simple) {
        JPanel controls = new JPanel();
        controls.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        controls.add(makeButtonWithText("C", canvas::clear));

        //controls.add(makeButtonWithText("E", canvas::startErasing));
        URL path = PaintGUI.class.getResource("/eraser-icon.png");
        ImageIcon icon = new ImageIcon(path.getPath());
        //JButton eraserButton = new JButton(icon);
        JCheckBox eraserButton = new JCheckBox(icon);
        eraserButton.setPreferredSize(BUTTON_SIZE.max(icon.getIconWidth(), icon.getIconHeight()).asDimension());
        final Color DESELECTED_BACKGROUND = eraserButton.getBackground();
        eraserButton.addActionListener(e -> {
            boolean state = eraserButton.isSelected();
            canvas.setErasing(state);
            eraserButton.setBackground(state ? Color.GRAY : DESELECTED_BACKGROUND);
            eraserButton.repaint();
        });
        //eraserButton.addActionListener(e -> canvas.setErasing(true));
        controls.add(eraserButton);

        controls.add(makeSliderPanel());

        if (simple) {
            for (Color c : COLOURS) {
                JButton colorButton = new JButton();
                colorButton.setPreferredSize(BUTTON_SIZE.asDimension());
                colorButton.setBackground(c);
                colorButton.addActionListener(e -> canvas.setColor(c));
                controls.add(colorButton);
            }
        } else {
            controls.add(makeButtonWithText("Color",
                    () -> canvas.setColor(
                            JColorChooser.showDialog(this, "Choose pen color", canvas.getColor()))));
        }

        return controls;
    }

    private JButton makeButtonWithText(String text, Runnable listener) {
        JButton button = new JButton(text);
        FontMetrics metrics = button.getFontMetrics(button.getFont());
        ImmutableDimension textSize =
                new ImmutableDimension(metrics.stringWidth(text), metrics.getHeight()).add(button.getInsets());
        button.setPreferredSize(BUTTON_SIZE.max(textSize).asDimension());
        button.addActionListener(e -> listener.run());
        return button;
    }

    private Component makeSliderPanel() {
        JPanel sliderPanel = new JPanel();

        JSlider slider = new JSlider(MIN_PEN_SIZE, MAX_PEN_SIZE);
        slider.setValue(canvas.getPenSize());
        JLabel label = new JLabel(String.valueOf(slider.getValue()));
        FontMetrics metrics = label.getFontMetrics(label.getFont());
        int width = IntStream.rangeClosed(MIN_PEN_SIZE, MAX_PEN_SIZE)
                .map(i -> metrics.stringWidth(String.valueOf(i)))
                .max().getAsInt();
        label.setPreferredSize(new Dimension(width, metrics.getHeight()));


        slider.addChangeListener(e -> {
            int value = slider.getValue();
            label.setText(Integer.toString(value));
            canvas.setPenSize(value);
        });

        sliderPanel.add(slider);
        sliderPanel.add(label);
        return sliderPanel;
    }

    public static void main(String[] args) {
        new PaintGUI(false);
    }
}