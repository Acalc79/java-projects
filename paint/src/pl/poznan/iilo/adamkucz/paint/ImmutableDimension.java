package pl.poznan.iilo.adamkucz.paint;

import java.awt.*;
import java.util.function.BiFunction;

public class ImmutableDimension {
    public final int width, height;

    public ImmutableDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Dimension asDimension() {
        return new Dimension(width, height);
    }

    public ImmutableDimension elementwise(ImmutableDimension other, BiFunction<Integer, Integer, Integer> f) {
        return new ImmutableDimension(f.apply(width, other.width), f.apply(height, other.height));
    }

    public ImmutableDimension max(ImmutableDimension other) {
        return elementwise(other, Math::max);
    }

    public ImmutableDimension max(int width, int height) {
        return max(new ImmutableDimension(width, height));
    }

    public ImmutableDimension min(ImmutableDimension other) {
        return elementwise(other, Math::min);
    }

    public ImmutableDimension min(int width, int height) {
        return min(new ImmutableDimension(width, height));
    }

    public ImmutableDimension add(Insets insets) {
        return add(insets.left + insets.right, insets.top + insets.bottom);
    }

    public ImmutableDimension add(int width, int height) {
        return add(new ImmutableDimension(width, height));
    }

    public ImmutableDimension add(ImmutableDimension other) {
        return elementwise(other, Integer::sum);
    }
}
