package pl.poznan.iilo.adamkucz.paint;

import javax.management.monitor.MonitorSettingException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.function.Function;

public class Canvas extends JPanel implements MouseListener, MouseMotionListener {
    private static final int ERASER_SIZE = 10;
    private final BufferedImage img;
    private Point lastPos;
    private Color color;
    private boolean erasing;
    private int size;

    public Canvas(int width, int height) {
        setPreferredSize(new Dimension(width, height));
        addMouseListener(this);
        addMouseMotionListener(this);
        color = Color.BLACK;
        size = 5;
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = img.createGraphics();
        g.setBackground(Color.WHITE);
        g.clearRect(0, 0, width, height);
        g.dispose();
        lastPos = null;
        erasing = false;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

    private void addPointOrErase(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        Graphics2D g = img.createGraphics();
        g.setBackground(Color.WHITE);
        g.setColor(color);
        if (lastPos == null) {
            if (!erasing) {
                g.fillOval(x - size, y - size, 2 * size, 2 * size);
            } else {
                g.clearRect(x - ERASER_SIZE, y - ERASER_SIZE, 2 * ERASER_SIZE, 2 * ERASER_SIZE);
            }
        } else {
            if (!erasing) {
                g.setStroke(new BasicStroke(2 * size, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                g.drawLine(lastPos.x, lastPos.y, x, y);
            } else {
                eraseSegment(g, lastPos, new Point(x, y));
            }
        }
        g.dispose();
        repaint();
        lastPos = new Point(x, y);
    }

    private void eraseSegment(Graphics g, Point start, Point finish) {
        Point pt = start;
        int dx = Integer.signum(finish.x - start.x);
        int dy = Integer.signum(finish.y - start.y);
        Line2D line = new Line2D.Double(start, finish);
        // functional
        Function<Point, Point> nextPoint;
        if (dx == 0 || dy == 0) {
            nextPoint = p -> new Point(p.x + dx, p.y + dy);
        } else {
            nextPoint = p -> {
                Point p1 = new Point(p.x + dx, p.y);
                Point p2 = new Point(p.x, p.y + dy);
                return line.ptLineDistSq(p1) < line.ptLineDistSq(p2) ? p1 : p2;
            };
        }
        do {
            g.clearRect(pt.x - ERASER_SIZE, pt.y - ERASER_SIZE, ERASER_SIZE * 2, ERASER_SIZE * 2);
            pt = nextPoint.apply(pt);
        } while (!pt.equals(finish));
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK) {
            addPointOrErase(e);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {
       if (e.getButton() == MouseEvent.BUTTON1) {
           addPointOrErase(e);
       }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        lastPos = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    public void clear() {
        Graphics g = img.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, img.getWidth(null), img.getHeight(null));
        g.dispose();
        repaint();
        revalidate();
    }

    public void setErasing(boolean newValue) {
        erasing = newValue;
    }

    public int getPenSize() {
        return size;
    }

    public void setPenSize(int value) {
        size = value;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
        erasing = false;
    }
}
